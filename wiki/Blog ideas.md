## Kubernetes
- [ ] Kubernetes operators
- [ ] Kubernetes storage

## Mini projects
- [ ] Finish my rust project for event processing
- [ ] Log generator from my masters thesis
- [ ] ETL (Extract Transaction Load) from my masters
- [ ] How is my second-brain made
- [ ] How is my personal website made
- [ ] a short summary of my masters thesis
- [ ] The lights out algorithm

## Rust
- [ ] Showcase rust crates
- [ ] Rust async crates vs sync (similar functionality)