| Package      | Tags | Description                                     | Url                                 |
| ------------ | ---- | ----------------------------------------------- | ----------------------------------- |
| Rust Quant   |      | Quantitative finance (data, distributions, ...) | https://github.com/avhz/RustQuant   |
| Color-eyre   |      | Colourful errors                                | https://github.com/yaahc/color-eyre |
| Bacon        |      | Automatic rebuilding and checking               | https://github.com/Canop/bacon      |
| Tracing      |      | Async logging                                   | https://github.com/tokio-rs/tracing |
| SQLx         |      | Compile time SQL checking                       | https://github.com/launchbadge/sqlx |
| Poem-openapi |      | Web framework, quickly write APIs, openapi      | https://github.com/poem-web/poem    |
| Serde        |      | Serialization                                   | https://github.com/serde-rs/serde   |
| Tokyo        |      | Low level async                                 | https://github.com/tokio-rs/tokio   |
